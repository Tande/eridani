import React from "react";
import IncrementalText from "../components/incrementalText";
import CharacterText from "../components/characterText";

export const defaultCommands = {
    examine: (state) => {
        const {userInput, getObjectNode, addToTextList} = state;
        const myObject = userInput
                            .toLowerCase()
                            .replace("examine ", "")
                            .replace(" ", "_");
        //todo: insert logic to remove dumb words like "the" "a" etc...
        var node = getObjectNode(myObject);
        addToTextList(node.text(state))
    }
}

//if something is multiple words, concat with _
export const Objects = {
    felix: { text: () => (
        <IncrementalText nodeID={"felix"} nextNode={false}>
            <CharacterText character="FELIX" emotion="NEUTRAL">"I'm jus' a fella, I guess."</CharacterText>
            As he says, just a normal fella, standing at a not-so-tall 5'0" exactly.  Bright blonde hair and a messy orbit of a man, maybe late twenties.  A salt of the 'Earth' type, Felix grew up on [PHOBOS] alongside his sister, [PAULA].
        </IncrementalText>) },
    gilese: { text: () => (
        <IncrementalText nodeID={"gilese"} nextNode={false}>
            An old ship.  Something of an antique given it was first constructed during the [MARS SURFACE WAR].  Originally used as a simple transport ship, it's been refitted multiple times and now is used as a junker for a simple scavenger crew.  It has its negatives to its age - such as missing many security upgrades (don't expect anything further than a simple numerical lock for your cabin room), but positives abound.  Such as the noise the exhaust fans make and the feeling that certainly there's a xenomorph aboard.
        </IncrementalText>
    ) },
    mars: { text: () => (
        <IncrementalText nodeID={"mars"} nextNode={false}>
            Not exactly home.  The big red planet was colonized maybe a good four dozen years ago by a group of Russian oligarchs.  It's gone back and forth between being property of the [USSR] and having its own independence.  The numerous set of battles from the drama that is the planet leaves quite a mess in orbit for folks in the [GILESE] to pick up, after all.
        </IncrementalText>
    ) },
    deimos: { text: () => {} },
    fridge: { text: () => {} },
    socket: { text: () => {} }
}